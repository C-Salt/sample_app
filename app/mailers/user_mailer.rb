class UserMailer < ApplicationMailer

  def account_activation(user)
    @user = user
    mail to: user.email, subject: "Account activation"
  end

=begin
  def password_reset
    @greeting = "Hi"

    mail to: "to@example.org"
  end
=end

  def password_reset(user)
    @user = user
    mail to: user.email, subject: "Password reset"
  end

end
